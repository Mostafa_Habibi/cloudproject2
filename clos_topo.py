#!/usr/bin/python

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.cli import CLI
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.util import irange,dumpNodeConnections
from mininet.log import setLogLevel
from mininet.node import RemoteController

import argparse
import sys
import time


class ClosTopo(Topo):

    def __init__(self, fanout, cores, **opts):
        # Initialize topology and default options
        Topo.__init__(self, **opts)
        self.coreList=[]
    	self.aggList=[]
    	self.edgeList=[]
    	self.hostList=[]
    	###########
    	self.cores=cores
    	self.fanout=fanout
    	self.agg=self.cores*self.fanout
    	self.edge=self.agg*self.fanout
    	self.host=self.edge*self.fanout
    	self.addCoreSwitch()
    	self.addAggSwitch()
    	self.addEdgeSwitch()
    	self.addHosts()
    	self.addLinks()

        #"Set up Core and Aggregate level, Connection Core - Aggregation level"

        for c in xrange (0,self.cores):
            self.coreList.append(self.addSwitch('c'+str(c+1)))

        for a in xrange (0,self.agg):
            self.aggList.append(self.addSwitch('a'+str(self.cores+a+1)))

        for i in xrange (0,self.cores):
    		for j in xrange (0,self.agg):
    			self.addLink(self.coreList[i],self.aggList[j])

        #"Set up Edge level, Connection Aggregation - Edge level "

        for e in xrange (0,self.edge):
            self.edgeList.append(self.addSwitch('e'+str(self.cores+self.agg+e+1)))

        for i in xrange (0,self.agg):
    		for j in xrange (0,self.edge):
    			self.addLink(self.aggList[i],self.edgeList[j])

        #"Set up Host level, Connection Edge - Host level "

        for h in xrange (0,self.host):
    		self.hostList.append(self.addHost('h'+str(h+1)))

        for i in xrange (0,self.host):
            self.addLink(self.edgeList[int(i/fanout)],self.hostList[i])



def setup_clos_topo(fanout=2, cores=1):
    "Create and test a simple clos network"
    assert(fanout>0)
    assert(cores>0)
    topo = ClosTopo(fanout, cores)
    net = Mininet(topo=topo, controller=lambda name: RemoteController('c0', "127.0.0.1"), autoSetMacs=True, link=TCLink)
    net.start()
    time.sleep(20) #wait 20 sec for routing to converge
    net.pingAll()  #test all to all ping and learn the ARP info over this process
    CLI(net)       #invoke the mininet CLI to test your own commands
    net.stop()     #stop the emulation (in practice Ctrl-C from the CLI
                   #and then sudo mn -c will be performed by programmer)


def main(argv):
    parser = argparse.ArgumentParser(description="Parse input information for mininet Clos network")
    parser.add_argument('--num_of_core_switches', '-c', dest='cores', type=int, help='number of core switches')
    parser.add_argument('--fanout', '-f', dest='fanout', type=int, help='network fanout')
    args = parser.parse_args(argv)
    setLogLevel('info')
    setup_clos_topo(args.fanout, args.cores)


if __name__ == '__main__':
    main(sys.argv[1:])
